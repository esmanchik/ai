import model

lines = model.read_lines("prep.csv")
print("%d lines" % len(lines))
train = lines[:20000]
# test = lines[20000:21000]

Xtrain, Ytrain = model.tensorize(train)
# Xtest, Ytest = model.tensorize(test)

print(Xtrain.shape, len(Ytrain), len(Ytrain[0]))

m = model.create()
m.fit(Xtrain, Ytrain, show_metric=True, batch_size=model.cell_count, n_epoch=12)
# m.fit(Xtrain, Ytrain, validation_set=(Xtest, Ytest), \
#       show_metric=True, batch_size=model.cell_count, n_epoch=12)

m.save("onet.tflearn")

titles = ["Software Developer", "Sales Agent", "Mathematical Teacher"]
x = model.vectorize(titles)
y = m.predict(x)

print([model.classf(mps[0]) for mps in map(model.max_positions, y)])

m.load("onet.tflearn")

titles = ["Senior Program Administrator", "Internship - Research Strategic Partnerships", "Facilities Manager", "Senior Recruiter", "Hardware development engineer"]
x = model.vectorize(titles)
y = m.predict(x)
print([model.classf(mps[0]) for mps in map(model.max_positions, y)])

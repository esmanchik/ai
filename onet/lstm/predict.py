import model
import numpy as np

m = model.create()
m.load("onet.tflearn")

test = model.read_lines("test.csv")
test.pop()

dicts = model.dictionarize(test)

def preproc(d):
    d["title"] = model.preproc(d["title"])
    d["desc"] = model.preproc(d["desc"])
    return d

dicts = map(preproc, dicts)

valid = [int(d["func"][0:2]) for d in dicts]

sentences = [d["title"] + " <end-of-title> " + \
             d["desc"] + " <end-of-sentence> " for d in dicts]
#print(sentences)

X = model.vectorize(sentences)

y = m.predict(X)

pred = [model.classf(mps[0]) for mps in map(model.max_positions, y)]

n = 0
for i, d in enumerate(dicts):
    print(pred[i], valid[i], d["func"][0:2], d["title"])
    if pred[i] == valid[i]:
        n = n + 1

print(float(n) / len(dicts))

v = map(lambda a, b: 1 if a == b else 0, valid, pred)
print(np.mean(v))
#print(v)


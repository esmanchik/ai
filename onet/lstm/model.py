from __future__ import division, print_function, absolute_import

from collections import Counter
import tensorflow as tf
import tflearn
from tflearn.data_utils import to_categorical, pad_sequences
from bs4 import BeautifulSoup
import re
import sys

reload(sys)
sys.setdefaultencoding('utf8')

input_count = 900
class_count = 23

def fclass(func):
    return (int(func) - 11) / 2

def classf(c):
    return c * 2 + 11

def read_lines(path):
    lines = []
    with tf.gfile.GFile(path, "r") as f:
        lines = f.read(100*1024*1024).split("\n")
    return lines

def preproc(text):
    text = BeautifulSoup(text, "html.parser").get_text(" ")
    # Remove \t and \n from mysql dump
    text = text.replace("\\n", " ").replace("\\t", " ").lower()
    text = re.sub("[^a-z]", " ", text)
    return text

words = read_lines("vocab.txt")
# append words to reserved word span
reserved = ["<unknown>", "<end-of-title>", "<end-of-sentence>"] 
words = reserved + ["<reserved>"] * (10 - len(reserved)) + words[:9990]
print("top 50 words:", words[:50]) 

word_to_id = dict(zip(words, range(len(words))))

print("%d words" % len(words))

def tokenize(text):
    words = text.lower().split()
    #    return [s.lower() for c in text.split(",") \
    #            for s in c.split(" ") if s != ""]
    return words

def wid(w):
    return word_to_id[w] if w in word_to_id else 0

def vectorize(sentences):
    wids = [map(wid, reversed(ts)) for ts in map(tokenize, sentences)]
    x = pad_sequences(wids, maxlen=input_count, value=0.)
    return x

def dictionarize(lines):
    dicts = []
    for line in lines:
        d = dict(zip(["func", "title", "desc"], line.split("\t")))
        dicts.append(d)
    return dicts

def tensorize(lines): 
    dicts = dictionarize(lines)

    funcs = [d["func"][0:2] for d in dicts]
    sentences = [d["title"] + " <end-of-title> " + \
                 d["desc"] + " <end-of-sentence> " for d in dicts]

    # test sentence index
    tsi = 7

    print(tokenize(sentences[tsi]))

    print(map(wid, tokenize(sentences[tsi])))

    x = vectorize(sentences)

    print(x[tsi])

    y = to_categorical(map(fclass, funcs), nb_classes=class_count)
    return (x, y)

cell_count = 192

def create():
    net = tflearn.input_data([None, input_count])
    net = tflearn.embedding(net, input_dim=10000, output_dim=cell_count)
    net = tflearn.lstm(net, cell_count, dropout=0.8)
    #net = tflearn.lstm(net, input_count, dropout=0.8)
    net = tflearn.dropout(net, 0.5)
    net = tflearn.fully_connected(net, class_count, activation='softmax')
    net = tflearn.regression(net, optimizer='adam', loss='categorical_crossentropy')

    model = tflearn.DNN(net, clip_gradients=0., tensorboard_verbose=0)
    return model

def max_positions(y):
    m = max(y)
    return [i for i, j in enumerate(y) if j == m]

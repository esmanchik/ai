from collections import Counter
import numpy as np
import model

path = "good.csv"
lines = []
with tf.gfile.GFile(path, "r") as f:
    lines = f.read().split("\n")
lines.pop()

print("%d lines" % len(lines))

dicts = []
funcs = []
sentences = []

prep = open("prep.csv", "w")

for line in lines:
    cols = line.split("\t")
    d = dict(zip(["func", "title", "desc"], cols))
    desc = model.preproc(d["desc"])
    sentence = d["title"] + desc
    sentences.append(sentence)
    funcs.append(d["func"][:2])
    prep.write("\t".join([d["func"], d["title"], desc]) + "\n")
    prep.flush()
    #dicts.append(d)

prep.close()

print("%d sentences" % len(sentences))

tokenized_sentences = map(model.tokenize, sentences)
sentence_lengths = map(len, sentences)
print("%d words in lagest sentence, %d mean" % (max(sentence_lengths), np.mean(sentence_lengths)))

words = [w for s in tokenized_sentences for w in s]
print("%d tokens" % len(words))

counter = Counter(words)
count_pairs = sorted(counter.items(), key=lambda x: (-x[1], x[0]))
words, _ = list(zip(*count_pairs))
#word_to_id = dict(zip(words, range(len(words))))

print("%d words" % len(words))

counter = Counter(funcs)
pairs = sorted(counter.items(), key=lambda x: (-x[1], x[0]))
print("function distribution:")
for p in pairs:
    print("%s: %d" % p)

vocab = open("vocab.txt", "w")
for word in words:
    vocab.write(word + "\n")
vocab.close()

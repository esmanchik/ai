import cv2
from espeak import espeak

clf = cv2.CascadeClassifier("classifier.xml")
i = 1
cap = cv2.VideoCapture(0)
while(True):
    if not cap.isOpened():
        cap = cv2.VideoCapture(0)
    ret, img = cap.read()
    #img = cv2.imread("cars%d.jpg" % i)
    print("frame #%d" % i)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    objs = clf.detectMultiScale(gray, 1.3, 5)
    #print(objs)
    for (x,y,w,h) in objs:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
    if len(objs) > 0:
        #print(dir(cap))
        cap.release()
        cv2.imwrite("image%d.jpeg" % i, img)
        cv2.imshow("Cars", img)
        espeak.synth("%d cars detected" % len(objs))
        cv2.waitKey()
    i = i + 1
    

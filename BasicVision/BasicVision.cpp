#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <mutex>
#include <chrono>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

class VideoProcessor {
  mutex mtx;
  Mat frame;
  bool last;
  VideoCapture cap;
  CascadeClassifier classifier;
  bool display;
public:
  VideoProcessor(VideoCapture capture) :
    classifier("/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml"), cap(capture), last(false), display(false) {}

  void set_display(bool val) {
    display = val;
  }
  
  void capture() {
    int i = 0;
    bool cont = true;
    while(cont) {
      Mat img;
      cap >> img;
      lock_guard<mutex> lock(mtx);
      frame = img;
      // cout << "got frame " << ++i << " "
      //     << frame.size().width << "x" << frame.size().height
      //     << endl;
      cont = !last;
    }
  }

  void process() {
    vector<Rect> objects;
    vector<vector<Point> > contours;
    if (display) namedWindow("Camera");
    for(int i = 0; i < 9; i++) {
      cout << "iteration " << i << endl;
      Mat image, gray;
      //cout << "acquire image "  << image.total() << endl;
      {
        lock_guard<mutex> lock(mtx);
        image = frame;
        last = i == 8;
      }
      cout << "got image "  << image.total() << endl;
      if (image.total() == 0) {
        this_thread::sleep_for(chrono::milliseconds(500));
        continue;
      }
      cvtColor(image, gray, CV_BGR2GRAY);
      
      classifier.detectMultiScale(gray, objects);
      cout << "found "  << objects.size() << " faces" << endl;
      if (objects.size() > 0) {
        cout << objects.size() << " faces detected" << endl;
        for(auto rect: objects) {
          rectangle(image, rect, cv::Scalar(255, 0, 0));
        }
      }

      threshold(gray, gray, 128, 255, CV_THRESH_BINARY);
      findContours(gray, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
      cout << "found " << contours.size() << " contours" << endl;

      // Draw the contours
      // cv::Mat contourImage(image.size(), CV_8UC3, cv::Scalar(0,0,0));
      cv::Scalar colors[3];
      colors[0] = cv::Scalar(255, 0, 0);
      colors[1] = cv::Scalar(0, 255, 0);
      colors[2] = cv::Scalar(0, 0, 255);
      int tough = 0;
      for (size_t idx = 0; idx < contours.size(); idx++) {
        Rect bounds = boundingRect(contours[idx]);
        if (bounds.area() * 8 > image.total()) {
          double epsilon = 0.01 * arcLength(contours[idx], true);
          approxPolyDP(Mat(contours[idx]), contours[idx], epsilon, true);
          drawContours(image, contours, idx, cv::Scalar(255, 255, 0));
          tough++;
        }
      }
      cout << tough << " tough contours" << endl;
      if(display) {
        cout << "displaying image" << endl;
        imshow("Camera", image);
      }
      stringstream filename;
      filename << "image" << i << ".jpg";
      imwrite(filename.str(), image);
    } 
  }
};

int main(int argc, char** argv) {
  if ( argc < 2 ) {
    cout << "usage: " << *argv <<" <Camera_Index>" << endl;
    return -1;
  }

  VideoCapture cap(stoi(string(argv[1])));
  if (!cap.isOpened()) {
    cout << "can not open camera with index " << argv[1] << endl;
    return -1;
  }

  VideoProcessor processor(cap);
  if (argc > 2) {
    processor.set_display(true);
    Mat test = imread("image.jpg");
    namedWindow("Test");
    imshow("Test", test);
  }
  
  thread capturer([&processor](){ processor.capture(); });
  thread detector([&processor](){ processor.process(); });

  capturer.join();
  detector.join();
  
  return 0;
}



